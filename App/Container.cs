﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Lync.Model;

namespace WpfApplication2
{
	public class Container
	{
		public LyncClient Lync { get; set; }

		private static Container _instance;
		public static Container Instance
		{
			get
			{
				if (_instance == null)
					_instance = new Container();

				return _instance;
			}
		}
	}
}
