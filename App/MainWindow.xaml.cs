﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Group;

namespace WpfApplication2
{
	public partial class MainWindow : Window
	{
		private const string TopGroupName = "top";
		private Dispatcher dispatcher;
		

		public MainWindow()
		{
			InitializeComponent();
			dispatcher = Dispatcher.CurrentDispatcher;

			//Loaded += Window_Loaded;
		}


		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			if (SetupLyncClient()) return;

			Group topGroup = Container.Instance.Lync.ContactManager.Groups.FirstOrDefault(g => g.Name.ToLower() == TopGroupName);
			if (topGroup == null)
			{
				Console.WriteLine("Top group cannot be found");
				return;
			}

			var topActions = new List<TopAction>();

			for (int i = 0; i < topGroup.Count; i++)
			{
				Contact contact = topGroup[i];

				var title = (string) contact.GetContactInformation(ContactInformationType.Title);
//				var description = (string) contact.GetContactInformation(ContactInformationType.Description);
				var contactsEndpoint =
					(ContactEndpoint) ((List<object>) contact.GetContactInformation(ContactInformationType.ContactEndpoints)).FirstOrDefault(c => (c as ContactEndpoint).Type == ContactEndpointType.WorkPhone);

				var topAction = new TopAction
					{
						Title = title,
						PhoneNumber = contactsEndpoint.DisplayName
					};

				topActions.Add(topAction);
			}

			topActions.Add(new TopAction
				{
					Other = true,
					Title = "Other..."
				});

			TopActionsList.Items.Clear();
			TopActionsList.ItemsSource = topActions;
		}

		private bool SetupLyncClient()
		{
			//Listen for events of changes in the state of the client
			try
			{
				Container.Instance.Lync = LyncClient.GetClient();
			}
			catch (ClientNotFoundException clientNotFoundException)
			{
				HandleException(clientNotFoundException);
				return true;
			}
			catch (NotStartedByUserException notStartedByUserException)
			{
				HandleException(notStartedByUserException);
				return true;
			}
			catch (LyncClientException lyncClientException)
			{
				Console.Out.WriteLine(lyncClientException);
				return true;
			}
			catch (SystemException systemException)
			{
				if (IsLyncException(systemException))
				{
					// Log the exception thrown by the Lync Model API.
					HandleException(systemException);
					return true;
				}

				// Rethrow the SystemException which did not come from the Lync Model API.
				throw;
			}

			Container.Instance.Lync.StateChanged += Client_StateChanged;
			return false;
		}

		private void HandleException(Exception ex)
		{
			Console.Out.WriteLine(ex);
		}

		private void Client_StateChanged(object sender, ClientStateChangedEventArgs e)
		{
			// dispatcher.BeginInvoke(new Action<ClientState>(UpdateUserInterface), e.NewState);
		}


		private
			bool IsLyncException(SystemException ex)
		{
			return
				ex is NotImplementedException ||
				ex is ArgumentException ||
				ex is NullReferenceException ||
				ex is NotSupportedException ||
				ex is ArgumentOutOfRangeException ||
				ex is IndexOutOfRangeException ||
				ex is InvalidOperationException ||
				ex is TypeLoadException ||
				ex is TypeInitializationException ||
				ex is InvalidComObjectException ||
				ex is InvalidCastException;
		}

		private void TopActionsList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count == 0)
				return;

			var item = (TopAction) e.AddedItems[0];
			if (item.Other)
			{
			}
			else
			{
				

				var timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 100)};
				timer.Tick += delegate(object o, EventArgs args)
					{
						TopActionsList.SelectedIndex = -1;
						PhoneToDial.Text = item.PhoneNumber.Substring(item.PhoneNumber.Length -4);
						timer.Stop();

						var sb = (Storyboard)TryFindResource("PhoneBegin");
						sb.Begin();
					};

				timer.Start();
			}
		}

		public class TopAction
		{
			public string Title { get; set; }
			public string PhoneNumber { get; set; }
			public bool Other { get; set; }
		}

		private void PhoneOverlay_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			PhoneOverlay.Visibility = Visibility.Collapsed;
			var sb = (Storyboard)TryFindResource("PhoneEnd");
			sb.Begin();
		}
	}
}